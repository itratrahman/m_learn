# Machine Learning Repository

## Description
This repository contains a sample of the code repository of "Machine Learning from Scratch" course by (c)Learner & Yearners. This is not the full repository of the course.

## Authors of this code repository
This repository is exclusively maintained and developed by `ITRAT RAHMAN` (CEO, Learners & Yearners).

## Take a look a our FREE COURSE uploaded in YouTube
[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/VNs7Jhf0rj4/0.jpg)](https://www.youtube.com/playlist?list=PLZSvmQjsBxbviHd2cRnuZNE7ctX6Sm6YD)
##### This free course covers the first chapter of the entire course

## Folder Description
This is the machine learning repository that contains the following folders:
- `m_learn`: this is the package which contains class files of low level implementations machine learning algorithms and other associated methods/functions.
- `analysis`: this folder contains all the jupyter notebooks of predictive analysis using our own m_learn package and the python scikit learn package.
- `examples`: Demonstrations of usage of python functions or methods we buid in our m_learn package or in jupyter notebook containing predictive analysis.

## License
This repository has BSD 3-clause License.
